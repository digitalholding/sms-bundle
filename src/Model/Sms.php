<?php

declare(strict_types=1);

namespace DH\SmsBundle\Model;

class Sms implements SmsInterface
{
    protected ?string $recipent = null;

    protected ?string $content = null;

    public function setRecipients(string $recipent): self
    {
        $this->recipent = $recipent;
        return $this;
    }

    public function getRecipient(): ?string
    {
        return $this->recipent;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;
        return $this;
    }
}
