<?php

declare(strict_types=1);

namespace DH\SmsBundle\Model;

interface SmsInterface
{
    public function setRecipients(string $recipent): self;

    public function getRecipient(): ?string;

    public function getContent(): ?string;

    public function setContent(string $content): self;
}
