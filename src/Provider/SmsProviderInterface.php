<?php

declare(strict_types=1);

namespace DH\SmsBundle\Provider;

use DH\SmsBundle\Model\SmsInterface;

interface SmsProviderInterface
{
    public function send(SmsInterface $sms): void;
}
