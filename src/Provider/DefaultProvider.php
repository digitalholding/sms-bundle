<?php

declare(strict_types=1);

namespace DH\SmsBundle\Provider;

use DH\SmsBundle\Model\SmsInterface;
use Smsapi\Client\Feature\Profile\Data\Profile;
use Smsapi\Client\Feature\Sms\Bag\SendSmsBag;
use Smsapi\Client\Service\SmsapiPlService;
use Smsapi\Client\SmsapiHttpClient;
use Symfony\Component\Translation\Provider\Dsn;

final class DefaultProvider implements SmsApiProviderInterface
{
    private SmsapiHttpClient $client;

    private string $config;

    private bool $customUri;

    public function __construct(
        SmsapiHttpClient $client,
        string $config,
        bool $customUri
    )
    {
        $this->client = $client;
        $this->config = $config;
        $this->customUri = $customUri;
    }

    public function send(SmsInterface $sms): void
    {
        $dsn = new Dsn($this->config);

        if(!$this->isSchemaConfigured($dsn)) {
            return;
        }

        $authToken = $this->getAuthToken($dsn);
        $from = $dsn->getRequiredOption('from');

        $sms = SendSmsBag::withMessage($sms->getRecipient(), $sms->getContent());
        $sms->from = $from;

        if (null !== $dsn->getOption('test') && 'true' === $dsn->getOption('test')){
            $sms->test = true;
        }

        $service = $this->getService($dsn, $authToken);
        $service->smsFeature()->sendSms($sms);
    }

    public function getProfile(): ?Profile
    {
        $dsn = new Dsn($this->config);

        if(!$this->isSchemaConfigured($dsn)) {
            return null;
        }

        $authToken = $this->getAuthToken($dsn);

        $additionalPrefix = null;

        if ($this->customUri === true) {
            $additionalPrefix = '/sms';
        }

        $service = $this->getService($dsn, $authToken, $additionalPrefix);

        return $service->profileFeature()->findProfile();
    }

    private function getService(
        Dsn $dsn,
        string $authToken,
        ?string $additionalPrefix = null
    ): SmsapiPlService
    {
        if (null !== $dsn->getOption('uri')) {
            $uri = $dsn->getOption('uri');

            if ($additionalPrefix !== null) {
                $uri = $dsn->getOption('uri') . $additionalPrefix;
            }

            return $this->client->smsapiPlServiceWithUri($authToken, $uri);
        }

        return  $this->client->smsapiPlService($authToken);
    }

    private function isSchemaConfigured(Dsn $dsn): bool
    {
        $scheme = $dsn->getScheme();

        if ('smsapi' !== $scheme) {
            return false;
        }

        return true;
    }

    private function getAuthToken(Dsn $dsn): string
    {
        return $dsn->getUser();
    }
}
