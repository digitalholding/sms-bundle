<?php

declare(strict_types=1);

namespace DH\SmsBundle\Provider;

use Smsapi\Client\Feature\Profile\Data\Profile;

interface SmsApiProviderInterface extends SmsProviderInterface
{
    public function getProfile(): ?Profile;
}
