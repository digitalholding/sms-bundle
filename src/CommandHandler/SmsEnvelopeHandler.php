<?php

declare(strict_types=1);

namespace DH\SmsBundle\CommandHandler;

use DH\SmsBundle\Command\SmsEnvelope;
use DH\SmsBundle\Model\Sms;
use DH\SmsBundle\Provider\SmsProviderInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final class SmsEnvelopeHandler
{
    /** @var SmsProviderInterface */
    private $defaultProvider;

    public function __construct(
        SmsProviderInterface $defaultProvider
    ) {
        $this->defaultProvider = $defaultProvider;
    }

    public function __invoke(SmsEnvelope $smsEnvelope): void
    {
        $provider = $smsEnvelope->getProvider();

        if (null === $provider) {
            $provider = $this->defaultProvider;
        }

        $sms = new Sms();
        $sms->setRecipients($smsEnvelope->getRecipent());
        $sms->setContent($smsEnvelope->getContent());

        $provider->send($sms);
    }
}
