<?php

declare(strict_types=1);

namespace DH\SmsBundle\Command;

use DH\SmsBundle\Model\SmsInterface;

class SmsEnvelope
{
    protected ?string $provider = null;

    protected string $recipent = '';

    protected string $content = '';

    public function __construct(
        string $recipent,
        string $content,
        ?string $provider = null
    )
    {
        $this->recipent = $recipent;
        $this->content = $content;
        $this->provider = $provider;
    }

    public function getProvider(): ?string
    {
        return $this->provider;
    }

    public function getRecipent(): string
    {
        return $this->recipent;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public static function fromNotification(SmsInterface $notification): self
    {
        return new self($notification->getRecipient(), $notification->getContent());
    }
}
