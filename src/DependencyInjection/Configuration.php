<?php

declare(strict_types=1);

namespace DH\SmsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('sms');
        $rootNode = $treeBuilder->getRootNode();

        $this->buildBundleConfigNode($rootNode);

        return $treeBuilder;
    }

    private function buildBundleConfigNode(ArrayNodeDefinition $rootNode): void
    {
        $rootNode
            ->children()
                ->scalarNode('custom_uri')
                    ->defaultTrue()
                    ->info('If true, custom uri will be added and sms prefix for profile')
                ->end()
                ->arrayNode('providers')
                    ->useAttributeAsKey('name')
                    ->prototype('scalar')->end()
                ->end()
            ->end()
        ;
    }
}
