<?php

declare(strict_types=1);

namespace DH\SmsBundle\Service;

use DH\SmsBundle\Command\SmsEnvelope;
use DH\SmsBundle\Model\SmsInterface;
use DH\SmsBundle\Provider\SmsProviderInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class SmsSendingService
{
    /** @var MessageBusInterface */
    private $messageBus;

    /** @var SmsProviderInterface */
    private $defaultProvider;

    public function __construct(
        MessageBusInterface $messageBus,
        SmsProviderInterface $defaultProvider
    ) {
        $this->messageBus = $messageBus;
        $this->defaultProvider = $defaultProvider;
    }

    public function send(
        SmsInterface $sms,
        ?SmsProviderInterface $provider = null
    ): void
    {
        if (null === $provider) {
            $provider = $this->defaultProvider;
        }

        $message = SmsEnvelope::fromNotification($sms);
        $this->messageBus->dispatch($message);
    }
}
